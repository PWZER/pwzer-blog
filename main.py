import os
import daemon
import yaml

import tornado
import tornado.web
import tornado.ioloop
import tornado.log
from tornado.options import options, define

define('port', default=8888, type=int, help='run on give port')
define('daemon', default=False, type=bool, help='run as a daemon process')

def start():
    handlers = [
            (r'/', IndexHandler),
            (r'/archive', ArchiveHandler)
            ]
    settings = {
            'template_path': os.path.join(os.path.dirname(__file__), 'templates'),
            'static_path': os.path.join(os.path.dirname(__file__), 'static')
            }
    application = tornado.web.Application(handlers=handlers, **settings)
    application.listen(options.port)
    try:
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.current().stop()


if __name__ == '__main__':
    options.parse_command_line()
    log_file = open('log/runing.log', 'a+', 0)
    if options.daemon:
        with daemon.DaemonContext(stdout=log_file, stderr=log_file):
            start()
    else:
        start()
